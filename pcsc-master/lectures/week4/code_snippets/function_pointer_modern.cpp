#include <functional>

double foo(double a) { return a + 1; }

int main() {

  std::function<double(double a)> ptr_foo = &foo;

  ptr_foo(10);
}